package com.api_demo.services;


import java.io.IOException;

import org.json.JSONObject;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.Pane;
import javafx.stage.Stage;


public class APIServices extends Application {

    static String imgURL = "";

    public static void imageData() throws IOException {

        StringBuffer response = new DataUrls().getResponseData();

        if (response != null) {
            JSONObject obj = new JSONObject(response.toString());
            JSONObject urlObject = obj.getJSONObject("urls");
            imgURL = urlObject.getString("small");
        } else{
            System.out.println("Respose is empty");
        }
    }

    @Override
    public void start(Stage primaryStage) throws Exception {
        imageData();

        Image image = new Image(imgURL);

        ImageView imageView = new ImageView(image);

        Pane pane = new Pane();
        pane.getChildren().addAll(imageView);

        Scene scene = new Scene(pane);
        primaryStage.setScene(scene);
        primaryStage.show();
    }


    

}
